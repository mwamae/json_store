import SideBar from  './layout/sidebar';
import TopBar from  './layout/topbar';
import PageBody from  './layout/body';
import PageFooter from  './layout/footer';
import GlobalSearch from  './search/search';

import Products from './products/product';


Vue.component('SideBar', SideBar);
Vue.component('TopBar', TopBar);
Vue.component('PageBody', PageBody);
Vue.component('PageFooter', PageFooter);
Vue.component('GlobalSearch', GlobalSearch);
Vue.component('Products', Products);

Vue.component('example-component', require('./ExampleComponent.vue').default);
