<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <top-bar inline-template>
        <header class="topbar" >
            <div class="topbar__logo">
                <img src="/img/logo.png" alt="">
                <span>Coalition Tech</span>
            </div>

            <div class="topbar__toggle">
                <a class="toggle_icon" @click="openMobileMenu">
                    <i class="material-icons">
                        menu
                    </i>
                </a>
            </div>
        </header>
    </top-bar>

    <side-bar inline-template>
        <aside class="sidebar" :class="{'collapsed': collapsed, 'mobile_active': openMobileMenuActive}">
            <div class="sidebar__logo">
                <img src="/images/logo.png" alt="">
                <span> Coalition Tech</span>
            </div>
            <div class="sidebar__menu">
                <ul>
                    <li>
                        <a href="/">
                            <i class="material-icons">filter_none</i>
                            <span>Products</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="sidebar__toggle" @click="toggleSidebar">
                <span class="toggle_text">Minimize</span>
                <span>
            <a class="toggle_icon">
                <i class="material-icons">chevron_left</i>
            </a>
        </span>
            </div>
        </aside>
    </side-bar>

    <page-body inline-template>
        <section class="playarea" :class="{'topbar_collapsed': topbarCollapsed}">
            <div class="playarea__content">
                @yield('content')
            </div>
        </section>
    </page-body>

    <page-footer inline-template>
        <footer class="footer_player">
           <p class="mx-5">  {{ date('Y') }} Coalition Technologies. Not under copyright</p>
        </footer>
    </page-footer>

</div>


</body>
</html>
