<?php

namespace Tests\Feature;

use App\JsonStorage\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductTest extends TestCase
{

    /**
     * Test product retrieval
     *
     * @return void
     */
    public function testProductRetrieval()
    {
        $product = [
            'name' => 'test product',
            'price' => 10,
            'quantity' => 5
        ];

        $product = $this->createProduct($product);

        $response = $this->get('/api/products');

        $response->assertStatus(200);
        $response->assertJsonStructure(['products', 'total']);


    }
    /**
     * Test creating products.
     *
     * @return void
     */
    public function testProductCreation()
    {
        $product = [
            'name' => 'test product',
            'price' => 10,
            'quantity' => 5
        ];
        $response = $this->post('/api/products', $product);

        $response->assertStatus(200);

        $this->assertTrue($this->products()->contains('name', 'test product'));
        $this->assertTrue($this->products()->contains('price', 10));
        $this->assertTrue($this->products()->contains('quantity', 5));
    }

    /**
     * Test editing products.
     *
     * @return void
     */
    public function testEditingAProduct()
    {
        $product = [
            'name' => 'test product',
            'price' => 10,
            'quantity' => 5
        ];

        $product = $this->createProduct($product);

        $response = $this->put('/api/products/update/'.$product['id'], [
            'name' => 'another test product',
            'price' => 20,
            'quantity' => 3
        ]);

        $response->assertStatus(200);

        $this->assertTrue($this->products()->contains('name', 'another test product'));
        $this->assertTrue($this->products()->contains('price', 20));
        $this->assertTrue($this->products()->contains('quantity', 3));
    }

    public function products(): \Illuminate\Support\Collection
    {
        $product = new Product();

        return $product->get();
    }

    public function createProduct($input) {
        $product = new Product();

        return $product->create($input);
    }
}
