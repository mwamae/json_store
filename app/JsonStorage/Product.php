<?php namespace App\JsonStorage;


use Carbon\Carbon;

class Product
{

    private $path = "database/products.json";


    public function create($array)
    {
        $array['id'] = $this->getNextProductId();
        $array['created_at'] = Carbon::now();

        $this->saveProduct($array);

        return $array;
    }


    public function update($array, $id)
    {
        $filteredProducts = $this->get()
            ->reject(function ($product) use ($id) {
                return $product['id'] == $id;
            });

        $product = $this->find($id);

        foreach ($array as $field => $value) {
            $product[$field] = $value;
        }

        $filteredProducts->push($product);

        $this->persistToFile($filteredProducts);

        return $product;
    }

    public function find($id)
    {
        $productsString = file_get_contents(base_path($this->path));

        //Convert products to collection for ease of manipulation
        $products = collect(json_decode($productsString, true));

        return $products->where('id', $id)->first();
    }

    public function get(): \Illuminate\Support\Collection
    {
        $productsString = file_get_contents(base_path($this->path));

        $products = json_decode($productsString, true);

        return collect($products);
    }

    private function getNextProductId(): int
    {
        $lastProductId = $this->get()->max('id');

        return $lastProductId + 1;
    }

    private function saveProduct($array)
    {
        $products = $this->get()->push($array);

        $this->persistToFile($products);
    }

    private function persistToFile($products)
    {
        $product = json_encode($products, JSON_PRETTY_PRINT);

        file_put_contents(base_path($this->path), stripslashes($products));

        return $product;
    }

}
