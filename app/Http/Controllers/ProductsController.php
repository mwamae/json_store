<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\JsonStorage\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{

    private $product;
    public function __construct(Product  $product)
    {
        $this->product = $product;
    }

    public function index()
    {
        $products = $this->product->get();

        $products = $products->map(function($product) {
            $product['value'] = $product['price'] * $product['quantity'];
            return $product;
        });

        $total = $products->sum('value');

        $products = $products->sortByDesc('created_at')->values()->all();

        return response()->json(['products' => $products, 'total' => $total], 200);
    }

    public function store(ProductRequest $request)
    {
        $input = $request->only([
            'name', 'price', 'quantity'
        ]);

        $product = $this->product->create($input);

        return response()->json($product, 200);
    }

    public function update(ProductRequest  $request, $id)
    {
        $input = $request->only([
            'name', 'price', 'quantity'
        ]);

        $product = $this->product->update($input, $id);

        return response()->json($product);
    }
}
